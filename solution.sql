-- Solution 1-a:
SELECT * FROM artists WHERE name LIKE  "%d%";

-- Solution 1-b:
SELECT * FROM songs WHERE length < 350;

-- Solution 1-c
SELECT album.album_title,songs.songs_name,songs.length FROM album JOIN songs ON album.id = songs.album_id


-- Solution 1-d
SELECT * FROM artists JOIN album ON artists.id = album.artist_id WHERE album_title LIKE "%a%"

-- Solution 1-e
SELECT * FROM album ORDER BY album_title DESC LIMIT 4;

-- Solution 1-f
SELECT * FROM album JOIN songs ON album.id = songs.album_id ORDER BY album_title DESC;